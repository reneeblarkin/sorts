package sorts;

import java.util.Arrays;
import java.util.Random;

public class InsertionSort {
	public static int[] insertionSort(int[] aList){
		for(int j = 0; j < aList.length; j++ ){
			int key = aList[j];
			int i = j-1;
			while( i >= 0 && aList[i]> key){
				aList[i+1] = aList[i];
				i =i-1;
			}
			aList[i+1] = key;
		}
		return aList;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] numbers = new int[9];
		Random random = new Random();
		for(int i=0 ; i< 9; i++){
			numbers[i] = random.nextInt(100);
		}
		System.out.println("This is the list of numbers: "+Arrays.toString(numbers));
		
		System.out.println("This is the list sorted"+Arrays.toString(insertionSort(numbers)));
	
	}
}
