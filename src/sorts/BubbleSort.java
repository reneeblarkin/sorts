package sorts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;


public class BubbleSort {
	
	private static int[] bubbleSort(int[] aList){
		for(int i =0; i< aList.length-1; i++){
			for(int j = i+1; j< aList.length; j++){
				if(aList[i] > aList[j]){
					int temp = aList[i];
					aList[i]= aList[j];
					aList[j] = temp;
				}
			}
		}
		return aList;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] numbers = new int[9];
		Random random = new Random();
		for(int i=0 ; i< 9; i++){
			numbers[i] = random.nextInt(100);
		}
		System.out.println("This is the list of numbers: "+Arrays.toString(numbers));
		
		System.out.println("This is the list sorted"+Arrays.toString(bubbleSort(numbers)));
	}

}
